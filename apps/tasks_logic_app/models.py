from django.utils import timezone
from django.db import models
from django.contrib.postgres.fields import JSONField

from apps.main_app.models import User


class Tag(models.Model):
    tag = models.CharField(max_length=30, unique=True)

class Role(models.Model):
    role = models.CharField(max_length=50, null=False)
    work_exp = models.IntegerField(null=True, default=0)

class Project(models.Model):
    stage = models.IntegerField(default=0, null=False)
    # 0 - opened stage
    # 1 - closed stage
    # 3 - finished stage
    is_complited = models.BooleanField(default=False, null=False)

    name = models.CharField(default='Без названия', max_length=20, null=True)
    short_description = models.CharField(max_length=200, null=True, blank=True)
    description = models.CharField(max_length=1000, null=False, blank=True)

    vcs_url = models.URLField(max_length=40, blank=True)
    technical_spec_url = models.URLField(max_length=40, blank=True)

    team_size = models.IntegerField(null=True, blank=True, default=20)
    team_lead = models.ForeignKey(User, on_delete=models.CASCADE)
    roles = JSONField(default=dict)

    tags = models.ManyToManyField(Tag)
    #требуемые специалисты
    wanted = models.ManyToManyField(Role)
    # В тегах показываются направление проекта, его тематика,
    # а также требуемые скиллы и навыки.

    """
    >>> Project.objects.create(name='example', roles={0: 'designer', 1: 'programmer'})
    >>> Project.objects.filter(roles__designer='Mark')
    """

    created     = models.DateTimeField(editable=False)
    modified    = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = timezone.now()
        # self.roles.update({self.team_lead.id : 'team_lead'})
        self.modified = timezone.now()
        return super(Project, self).save(*args, **kwargs)

class Star(models.Model):
    project = models.ForeignKey(Project, on_delete=models.PROTECT, null=False)
    persona = models.ForeignKey(User, on_delete=models.PROTECT, null=False)
    role = models.CharField(max_length=50, null=False)

