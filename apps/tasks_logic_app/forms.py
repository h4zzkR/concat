from apps.tasks_logic_app.models import Project

from django import forms


class ProjectForm(forms.ModelForm):
    name = forms.CharField(required=False, max_length=25,
                           widget=forms.TextInput(attrs={'placeholder': 'Название Вашего проекта',
                                                         'id': 'input-name',
                                                         "minlength": '3',
                                                         "maxlength": '25',
                                                         'name': 'name',
                                                         'class': 'form-control form-control-alternative',
                                                         'onkeyup': 'saveValue(this);'}), initial='Проект без названия')

    short_description = forms.CharField(required=True, max_length=300,
                                        widget=forms.Textarea(attrs={
                                            'placeholder': 'Как бы вы описали Ваш проект? Старайтесь писать лаконично.',
                                            'id': 'input-short_description',
                                            "minlength": '20',
                                            "maxlength": '300',
                                            "rows": "10",
                                            'name': 'short_description',
                                            'class': 'form-control form-control-alternative',
                                            'onkeyup': 'saveValue(this);'}), initial='')

    description = forms.CharField(required=True, max_length=5000,
                                  widget=forms.Textarea(attrs={
                                      'placeholder': 'Что конкретно Вы ходите сделать? Подробно опишите идею и цели Вашего проекта.' + \
                                                     ' Вы сможете редактировать это поле в дальнейшем.',
                                      'id': 'input-description',
                                      "minlength": '500',
                                      "maxlength": '5000',
                                      "rows": "4",
                                      'name': 'description',
                                      'class': 'form-control form-control-alternative',
                                      'onkeyup': 'saveValue(this);'}), initial='')

    vcs_url = forms.URLField(required=False, max_length=50,
                             widget=forms.URLInput(attrs={'placeholder': "Ссылка на репозиторий (если есть)",
                                                          'id': 'input-vcs_url',
                                                          'name': 'vcs_url',
                                                          "minlength": '10',
                                                          "maxlength": '50',
                                                          'class': 'form-control form-control-alternative',
                                                          'onkeyup': 'saveValue(this);'}))

    technical_spec_url = forms.URLField(required=False, max_length=50,
                                        widget=forms.URLInput(attrs={'placeholder': "Ссылка на ТЗ (если есть)",
                                                                     'id': 'input-ts_url',
                                                                     'name': 'ts_link',
                                                                     "minlength": '10',
                                                                     "maxlength": '50',
                                                                     'class': 'form-control form-control-alternative',
                                                                     'onkeyup': 'saveValue(this);'}))  # only google.docs

    team_size = forms.IntegerField(widget=forms.TextInput(attrs={'type' : 'number',
                                                                 'id': 'input-team_size',
                                                                 'name': 'team_size',
                                                                 "min": '2',
                                                                 "value": '2',
                                                                 "max": '50',
                                                                 'class': 'form-control form-control-alternative',
                                                                 'onkeyup': 'saveValue(this);'}))

    # roles = forms.CharField(required=True, max_length=500, widget=forms.TextInput(attrs={'placeholder': '',
    #                                                        'id': 'input-roles',
    #                                                        "minlength": '3',
    #                                                        "maxlength": '500',
    #                                                        'name': 'roles',
    #                                                        'class': 'form-control form-control-alternative',
    #                                                        'onkeyup': 'saveValue(this);'}))

    tags = forms.CharField(required=True, max_length=500, widget=forms.TextInput(attrs={'placeholder': 'Список тегов',
                                                                                        'id': 'input-tags',
                                                                                        "minlength": '3',
                                                                                        "maxlength": '500',
                                                                                        'name': 'tags',
                                                                                        'class': 'form-control form-control-alternative',
                                                                                        'onkeyup': 'saveValue(this);'}))

    class Meta:
        model = Project
        fields = ('name',
                  'short_description', 'description', 'vcs_url',
                  'technical_spec_url', 'team_size')

    def clean(self):
        messages = []
        name = self.cleaned_data['name']
        sd = self.cleaned_data['short_description']
        warnings = []
        try:
            d = self.cleaned_data['description']
        except KeyError:
            warnings.append('Описание проекта должно быть длиннее 300 символов')
        vcs = self.cleaned_data['vcs_url']
        ts = self.cleaned_data['technical_spec_url']
        team_size = self.cleaned_data['team_size']
        try:
            tags = self.cleaned_data['tags']
        except KeyError:
            pass
        print(tags)

        # if 'https://github.com/' not in github_link:
        #     warnings += ['Неправильная ссылка/домен на github!']
        # if 'https://gitlab.com/' not in gitlab_link:
        #     warnings += ['Неправильная ссылка/домен на gitlab!']
        # if 'https://t.me/' not in telegram_link:
        #     warnings += ['Неправильная ссылка/домен на telegram!']
        # for z in (first_name, last_name):
        #     if len(z) != 0:
        #         if any(char.isdigit() for char in z):
        #             warnings += ['В имени/фамилии должны быть только буквы!']


        # self.cleaned_data.update({'country' : country})
        # self.cleaned_data.update({'city' : city})

        # return warnings


