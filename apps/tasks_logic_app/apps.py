from django.apps import AppConfig


class TasksLogicAppConfig(AppConfig):
    name = 'apps.tasks_logic_app'
