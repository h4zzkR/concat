from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
import apps.tasks_logic_app.forms as forms
from apps.main_app.views import get_context


def render_project(request, id):
    id = int(id)

@login_required
def new_project(request):
    context = get_context(request, 'Создать проект')
    if request.method == 'POST':
        user = request.user
        form = forms.ProjectForm(request.POST, instance=user)
        if form.is_valid():
            form.clean()
            # warnings = form.clean_renewal_date()
        # print(form)
        return redirect(request, 'index.html')
    elif request.method == 'GET':
        form = forms.ProjectForm()
        context = get_context(request, 'Создать проект')
        context.update({'form' : form, 'state' : 'create'})

        return render(request, 'project_setup.html', context)
        # return request, context
    #     form = tnpf.ProfileForm(request.POST, instance=user)
    #     if form.is_valid():
    #         form.clean()
    #         warnings = form.clean_renewal_date()
    #         if len(warnings) != 0:
    #             print(warnings)
    #             response_data = {}
    #             for m in warnings:
    #                 messages.error(request, m, extra_tags='safe')
    #             response_data.update({'result': 'error'})
    #             response_data.update({'messages': ajax_messages(request)})
    #             return HttpResponse(
    #                 json.dumps(response_data),
    #                 content_type="application/json"
    #             )
    #         form.save()
    #         user.city = form.data['state']; user.save()
    #         response_data = {'username' : user.username,
    #                          'first_name' : user.first_name,
    #                          'last_name' : user.last_name,
    #                          'country' : user.country,
    #                          'state' : user.city, 'github_link' : user.github_link,
    #                          'gitlab_link' : user.gitlab_link, 'telegram_link' : user.telegram_link,
    #                          'linkedin_link' : user.linkedIn_link, 'bio' : user.bio}
    #         response_data.update({'result' : 'successful!'})
    #         messages.success(request, '<i class="em em---1"></i>', extra_tags='safe')
    #         response_data.update({'messages' : ajax_messages(request)})
    #
    #         return HttpResponse(
    #             json.dumps(response_data),
    #             content_type="application/json"
    #         )
    #
    #     else:
    #         parse_errors(request, form.errors)
    #         response_data = {}
    #         response_data.update({'messages': ajax_messages(request)})
    #         print(form.errors, response_data)
    #
    #         return HttpResponse(
    #             json.dumps(response_data),
    #             content_type="application/json"
    #         )
    #
    # else:
    #     pass