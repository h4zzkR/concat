from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
import apps.profile_app.forms as tnpf
import json
from django.http import HttpResponse
from apps.main_app.views import get_context, update_avatar, get_avatar, ajax_messages
from apps.main_app.models import User


def init_fields(obj, form):
    fields = list(form.fields)
    for f in fields:
        if f != 'city' and f != 'country':
            form.fields[f].initial = getattr(obj, f)
    return form

def update_model(obj, form_data):
    for key in form_data.keys():
        field = obj._meta.get_field(key)
        field = form_data[key]
    obj.save()


def test(request):
    context = get_context(request, 'test')
    return render(request, 'test.html', context)

@login_required
def render_profile(request, hash):
    user = User.objects.get(hash=hash)
    if user.is_confirmed == False:
        messages.warning(request, 'Ваш аккаунт неверефицирован. Некоторые функции могут быть недоступны.')
    context = get_context(request, 'Профиль')
    context.update({'user': user})
    context.update({'picture': user.picture})
    context.update({'form': init_fields(user, tnpf.ProfileForm())})
    return render(request, 'profile.html', context)

def parse_errors(request, errs):
    for e in str(errs).split('</li></ul></li>'):
        print('E', e)
        if 'Enter a valid URL' in e:
            if 'telegram' in e: obj = 'telegram'
            elif 'github' in e: obj = 'github'
            elif 'gitlab' in e: obj = 'gitlab'
            msg = f'Неправильная ссылка на {obj}!'
            messages.error(request, msg, extra_tags='safe')

@login_required
def gen_con(request):
    user = request.user
    avatar = get_avatar()
    update_avatar(avatar, user)
    return redirect(f'/u/{user.hash}', request)

@login_required
def update_profile(request):
    context = get_context(request, 'Профиль')
    if request.method == 'POST':
        email = request.POST.get('email')
        user = User.objects.get(email=email)
        form = tnpf.ProfileForm(request.POST, instance=user)
        if form.is_valid():
            form.clean()
            warnings = form.clean_renewal_date()
            if len(warnings) != 0:
                print(warnings)
                response_data = {}
                for m in warnings:
                    messages.error(request, m, extra_tags='safe')
                response_data.update({'result': 'error'})
                response_data.update({'messages': ajax_messages(request)})
                return HttpResponse(
                    json.dumps(response_data),
                    content_type="application/json"
                )
            form.save()
            user.city = form.data['state']; user.save()
            response_data = {'username' : user.username,
                             'first_name' : user.first_name,
                             'last_name' : user.last_name,
                             'country' : user.country,
                             'state' : user.city, 'github_link' : user.github_link,
                             'gitlab_link' : user.gitlab_link, 'telegram_link' : user.telegram_link,
                             'linkedin_link' : user.linkedIn_link, 'bio' : user.bio}
            response_data.update({'result' : 'successful'})
            messages.success(request, '<i class="em em---1"></i>', extra_tags='safe')
            response_data.update({'messages' : ajax_messages(request)})

            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
            )

        else:
            parse_errors(request, form.errors)
            response_data = {}
            response_data.update({'messages': ajax_messages(request)})
            print(form.errors, response_data)

            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
            )

    else:
        pass
    # return render(request, 'test.html', context)
