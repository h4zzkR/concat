from django.apps import AppConfig


class ProfileAppConfig(AppConfig):
    name = 'apps.profile_app'
