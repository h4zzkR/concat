from apps.main_app.models import User

from django import forms


class ProfileForm(forms.ModelForm):
    username = forms.CharField(required=False, max_length=15, widget=forms.TextInput(attrs={'placeholder': 'Никнейм',
                                                             'id': 'input-username',
                                                            "minlength": '2',
                                                            "maxlength": '15',
                                                             'name': 'username',
                                                             'class': 'form-control form-control-alternative',
                                                           'onkeyup': 'saveValue(this);'}))

    email = forms.CharField(required=False, widget=forms.EmailInput(attrs={'placeholder': 'Почта',
                                                           'id': 'input-email',
                                                           'name': 'email',
                                                           "data-validation": "email",
                                                            'class': 'form-control form-control-alternative',
                                                           'onkeyup':'saveValue(this);',
                                                           'readonly' : True,}))

    first_name = forms.CharField(required=False, max_length=15, widget=forms.TextInput(attrs={'placeholder': "Укажите ваше имя, если нужно",
                                                                 'label': 'first_name',
                                                                 'id' : 'input-first-name',
                                                                  "minlength": '2',
                                                                  "maxlength": '15',
                                                                 'name': 'first_name',
                                                                 'class': 'form-control form-control-alternative',
                                                                 'onkeyup':'saveValue(this);'}))

    last_name = forms.CharField(required=False, max_length=15, widget=forms.TextInput(attrs={'placeholder': "Укажите вашу фамилию, если нужно",
                                                                 'label': 'last_name',
                                                                 'id' : 'input-last-name',
                                                                 "minlength": '2',
                                                                 "maxlength": '15',
                                                                 'name': 'last_name',
                                                                 'class': 'form-control form-control-alternative',
                                                                 'onkeyup':'saveValue(this);'}))

    github_link = forms.URLField(required=False, max_length=30, widget=forms.URLInput(attrs={'placeholder': "Ссылка на ваш профиль",
                                                                 'id' : 'input-github',
                                                                 'name' : 'github_url',
                                                                 "data-validation": "url length",
                                                                 "data-validation-length": "min3 max33",
                                                                 'class': 'form-control form-control-alternative',
                                                                 'onkeyup':'saveValue(this);'}))


    gitlab_link = forms.URLField(required=False, max_length=30,  widget=forms.URLInput(attrs={'placeholder': "Ссылка на ваш профиль",
                                                                 'id' : 'input-gitlab',
                                                                 'name' : 'gitlab_url',
                                                                  "minlength": '3',
                                                                  "maxlength": '33',
                                                                 'class': 'form-control form-control-alternative',
                                                                 'onkeyup':'saveValue(this);'}))

    telegram_link = forms.URLField(required=False, max_length=30, widget=forms.URLInput(attrs={'placeholder': "Ссылка на ваш Telegram",
                                                                 'id' : 'input-telegram',
                                                                 'name' : 'telegram_url',
                                                               "minlength": '3',
                                                               "maxlength": '23',
                                                                 'class': 'form-control form-control-alternative',
                                                                 'onkeyup':'saveValue(this);'}))

    linkedIn_link = forms.URLField(required=False, max_length=30, widget=forms.URLInput(attrs={'placeholder': "Ссылка на ваш LinkedIn",
                                                                 'id' : 'input-linkedin',
                                                                 'name' : 'linkedin_url',
                                                                 "minlength" : '3',
                                                                 "maxlength" : '33',
                                                                 'class': 'form-control form-control-alternative',
                                                                 'onkeyup':'saveValue(this);'}))


    bio = forms.CharField(required=False, max_length=400, widget=forms.Textarea(attrs={'placeholder': "Несколько слов о вас ...",
                                                                 "rows" : "4",
                                                                 'id' : 'bio',
                                                                  "maxlength" : "200",
                                                                 'name' : 'bio',
                                                                 'class': 'form-control form-control-alternative',
                                                                 'onkeyup':'saveValue(this);'}))

    class Meta:
        model = User
        fields = ('username', 'first_name',
                  'last_name', 'github_link', 'gitlab_link',
                  'telegram_link', 'linkedIn_link', 'bio', 'city',
                  'country')

    def clean_renewal_date(self):
        username = self.cleaned_data['username']
        first_name = self.cleaned_data['first_name']
        last_name = self.cleaned_data['last_name']
        github_link = self.cleaned_data['github_link']
        gitlab_link = self.cleaned_data['gitlab_link']
        telegram_link = self.cleaned_data['telegram_link']
        linkedIn_link = self.cleaned_data['linkedIn_link']
        bio = self.cleaned_data['bio']
        country = self.data['country']


        warnings = []

        if country == '-1':
            country = 'не указано'
        city = self.data['state']

        if 'https://github.com/' not in github_link:
            warnings += ['Неправильная ссылка/домен на github!']
        if 'https://gitlab.com/' not in gitlab_link:
            warnings += ['Неправильная ссылка/домен на gitlab!']
        if 'https://t.me/' not in telegram_link:
            warnings += ['Неправильная ссылка/домен на telegram!']
        for z in (first_name, last_name):
            if len(z) != 0:
                if any(char.isdigit() for char in z):
                    warnings += ['В имени/фамилии должны быть только буквы!']


        self.cleaned_data.update({'country' : country})
        self.cleaned_data.update({'city' : city})

        return warnings
        # return {'username' : username, 'first_name' : first_name, 'last_name' : last_name, \
        #     'github_link' : github_link, 'gitlab_link' : gitlab_link, 'telegram_link' : telegram_link, \
        #     'linkedIn_link' : linkedIn_link, 'bio' : bio, 'country' : country, 'city' : city
        #         }, warnings



