from django.contrib.auth import login as auth_login

CHECK_MAIL = False

from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import User

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

class LoginForm(forms.Form):
    email = forms.CharField(widget=forms.EmailInput(attrs={'placeholder': 'Email',
                                                                 'required': True,
                                                                 'label': 'email',
                                                                 'name': 'email',
                                                                 'class': 'form-control'}))

    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Пароль',
                                                                 'required': True,
                                                                 'label': 'password',
                                                                 'name': 'password',
                                                                 'class': 'form-control'}))

    remember = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={
                                                             'name':'remember', 'id': 'customCheckLogin',
                                                              'class': 'custom-control-input'}))

    def clean_renewal_date(self):
        password = self.cleaned_data['pasword']
        email = self.cleaned_data['email']
        try:
            remember = self.cleaned_data['remember']
        except: remember = False

        return password, email, remember


class RegisterForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Ник',
                                                                 'required': True,
                                                                 'label': 'email',
                                                                 'name': 'email',
                                                                 'class': 'form-control',
                                                           'onkeyup':'saveValue(this);'}))

    email = forms.CharField(widget=forms.EmailInput(attrs={'placeholder': 'Email',
                                                                 'required': True,
                                                                 'label': 'email',
                                                                 'name': 'email',
                                                                 'class': 'form-control',
                                                           'onkeyup':'saveValue(this);'}))

    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Пароль',
                                                                 'required': True,
                                                                 'id': 'password',
                                                                 'label': 'password',
                                                                 'name': 'password',
                                                                 'class': 'form-control'}))

    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Еще раз пароль',
                                                                 'required': True,
                                                                 'id': 'confirm_password',
                                                                 'label': 'confirm_password',
                                                                 'name': 'confirm_password',
                                                                 'class': 'form-control'}))

    agreed = forms.BooleanField(widget=forms.CheckboxInput(attrs={
        'id': 'customCheckRegister',
        'required': True,
        'class': 'custom-control-input'}))

    def clean_renewal_date(self):
        from email_validator import validate_email, EmailNotValidError
        password = self.cleaned_data['pasword']
        email = self.cleaned_data['email']
        remember = self.cleaned_data['remember']
        username = self.cleaned_data['username']

        if len(password) < 6:
            raise ValidationError(_('Длина вашего пароля должна быть больше 6 символов'))

        if CHECK_MAIL is True:
            try:
                v = validate_email(email)  # validate and get info
            except EmailNotValidError as e:
                raise ValidationError(_('Такой почты не существует. Убедитесь, что ящик реален.'))

        return username, email, password

    def send_or_confirm_mail(self, request, user, is_debug):
        if is_debug is True:
            state = user.send_confirm_token(request)
            auth_login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        else:
            user.is_confirmed = True
            user.save()

class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = User
        fields = ('username', 'email')

class CustomUserChangeForm(UserChangeForm):

    class Meta(UserChangeForm):
        model = User
        # fields = UserChangeForm.Meta.fields

        fields = ('username', 'email', 'first_name', 'last_name',
                  'bio', 'city', 'country', 'is_active', 'picture', 'is_admin',
                  'is_staff')