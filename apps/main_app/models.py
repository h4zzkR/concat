from django.db import models
from django.utils.timezone import now
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from modules.user_manager import UserManager
from django.conf import settings

import modules.some_values as trash

from django.template.loader import get_template, render_to_string
from django.core.mail import send_mail, EmailMessage

from django.contrib.auth.models import PermissionsMixin

import os

import datetime
from modules import stuff


class User(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(_('email'), unique=True, blank=True, null=True)
    username = models.CharField(max_length=30, unique=False, null=False)
    hash = models.CharField(max_length=37, default='0')
    picture = models.ImageField(upload_to='users/avatars/', default='users/avatars/default.png', null=True, blank=True)

    first_name = models.CharField(max_length=30,)
    last_name = models.CharField(max_length=30,)

    rate = models.IntegerField(default=0)

    bio = models.TextField(max_length=500, blank=True)
    country = models.CharField(max_length=20, blank=True, default='не указано')
    city = models.CharField(max_length=30, blank=True, default='не указано')
    projects_cnt = models.IntegerField(default=0)
    stars_cnt = models.IntegerField(default=0)
    rate = models.FloatField(default=0.0)

    linkedIn_link = models.URLField(max_length=40, blank=True)
    github_link = models.URLField(max_length=40, blank=True, default='https://github.com/')
    telegram_link = models.URLField(max_length=40, blank=True, default="https://t.me/")
    gitlab_link = models.URLField(max_length=40, blank=True, default="https://gitlab.com/")
    bitbucket_link = models.URLField(max_length=40, blank=True, default="https://bitbucket.org/")

    date_joined = models.DateTimeField(_('registered'), default=now, editable=False)
    is_active = models.BooleanField(_('is_active'), default=True)

    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    is_confirmed = models.BooleanField(default=False)
    is_from_telegram = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = [email]

    #TODO Добавить сферы интереса, опыт работы.

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def find_city(self):
        try:
            return trash.cities.index((self.city, self.city))
        except ValueError:
            return trash.cities.index(("не указано", "не указано"))

    def find_country(self):
        try:
            return trash.countries.index((self.country, self.country))
        except ValueError:
            return trash.countries.index(("не указано", "не указано"))

    def email_user(self, subject, template_name, context, sender='concat.no.reply@gmail.com'):
        msg_html = render_to_string(template_name, context)
        msg = EmailMessage(subject=subject, body=msg_html, from_email=sender, bcc=[self.email])
        msg.content_subtype = "html"  # Main content is now text/html
        return msg.send()

    def confirm_account(self):
        self.is_confirmed = True
        self.save()

    def send_confirm_token(self, request):
        token_object = Token.objects.create(user=self)
        token = token_object.token

        context = {'username': self.username}
        context.update({'link': stuff.get_url_path(request) + '/confirm' + '?token=' + str(token)})
        context.update({'domain': 'concat.net'})
        context.update({'unsub': stuff.get_url_path(request) + '/confirm' + '?id=' + str(self.id)})

        return self.email_user('Верификация аккаунта', 'mail/concat_verify.html', context)

    def purge(self):
        os.remove(settings.MEDIA_ROOT + '/users/avatars/' + self.picture)
        self.delete()


class Token(models.Model):
    token = models.CharField(max_length=30, default=stuff.token_generator(), null=False)
    user = models.ForeignKey(to=User, blank=True, on_delete=models.CASCADE, null=True)
    creation_date = models.CharField(max_length=20, default=datetime.datetime.now().time(),
                                     null=False)