from allauth.socialaccount.models import SocialAccount
from allauth.socialaccount.signals import pre_social_login
from django.contrib import auth
from django.dispatch import receiver
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import render, redirect
import uuid

from apps.main_app.forms import *
from modules import stuff
from modules.stuff import public, parse_google_data

#README: public is antonym of login_required

SESSION_COOKIE_AGE = 60 * 60 * 6
DEPLOY = False

messages_tags = {
    25 : 'alert-success',
    20 : 'alert-info',
    10 : 'alert-info',
    30 : 'alert-warning',
    40 : 'alert-danger'
}

if not DEPLOY:
    REG_CONF = False
    CHECK_MAIL = False
else:
    REG_CONF = True
    CHECK_MAIL = True



def get_context(request, pagename):
    context = {
        'pagename': pagename,
        'user': request.user
    }

    # social_info = SocialAccount.objects.filter(user=request.user)
    # data = social_info.values('extra_data')

    if request.user.is_authenticated:
        picture = request.user.picture
        context.update({'picture': picture})

    return context


def ajax_messages(request):
    django_messages = []

    for message in messages.get_messages(request):
        django_messages.append({
            "level": messages_tags[message.level],
            "message": message.message,
            "extra_tags": message.tags,
        })

    return django_messages


def update_avatar(pil_obj, user_obj):
    try:
        from StringIO import StringIO
    except ImportError:
        from io import BytesIO
    from django.core.files.base import ContentFile
    new_avatar = BytesIO()
    try:
        pil_obj.save(new_avatar, format='png')
        s = new_avatar.getvalue()
        user_obj.picture.save(user_obj.picture,
                              ContentFile(s))
        user_obj.save()
    except TypeError:
        # if user_obj has no avatar
        user_obj.picture.save(f'{str(user_obj.id)}_av.png', ContentFile(s))
    finally:
        new_avatar.close()


@receiver(pre_social_login)
def connect_user(request, **kwargs):
    """
    Связывание аккаунтов с одинаковой почтой
    """
    from allauth.account.models import EmailAddress
    sociallogin = kwargs['sociallogin']
    user = sociallogin.user
    if user.id:
        return
    try:
        user = User.objects.get(
            email=user.email)
        sociallogin.state['process'] = 'connect'
        sociallogin.connect(request, user)
    except User.DoesNotExist or EmailAddress.DoesNotExist:
        pass


def third_accs_control(request):
    return redirect('/accounts/social/connections/', request)


def update_fields(request):
    user = request.user
    if str(user.picture) == 'users/avatars/default.png':
        s_info = SocialAccount.objects.filter(user=request.user)
        data = s_info.values('extra_data')
        data = parse_google_data(data)
        update_avatar(data[0], request.user)
        user.save()
        if data[1] != 'google':
            user.github_link = data[1]
            user.save()

    if str(user.hash) == '0':
        s_info = SocialAccount.objects.filter(user=request.user)
        data = s_info.values('extra_data')
        data = parse_google_data(data)
        user.hash = uuid.uuid4()
        user.is_confirmed = True
        user.save()
        if data[1] != 'google':
            user.github_link = data[1]
            user.save()

    return redirect('/')


def update_fields_telegram(request):
    data = request.GET
    first_name = data['first_name']
    last_name = ''
    try:
        last_name = data['last_name']
    except:
        last_name = ''
    username = data['username']
    id = data['id'];
    user = None
    try:
        user = User.objects.get(hash=id)
    except User.DoesNotExist:
        pic = stuff.get_tg_pic(data['photo_url'])
        user = User(username=username, first_name=first_name, last_name=last_name)
        user.is_confirmed = True;
        user.is_from_telegram = True;
        user.hash = data['id']
        user.email = f'telegram{id}@concat.mail';
        user.telegram_link += username;
        update_avatar(pic, user);
        user.save()

    auth.login(request, user, backend='django.contrib.auth.backends.ModelBackend')
    return redirect("/")


def get_post(request):
    return {k: request.POST.get(k) for k in request.POST.keys()}


def get_avatar():
    hash = stuff.avatar_generator()
    return avatars.Identicon(hash).generate()


def index(request):
    if request.user.is_authenticated:
        context = get_context(request, 'Хаб')
        return render(request, 'index.html', context)
    else:
        context = get_context(request, 'greetings')
        return render(request, 'greetings.html', context)

def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid():
            form.clean()
            password = form.cleaned_data['password']
            remember = form.cleaned_data['remember']
            email = form.cleaned_data['email']
            user = auth.authenticate(username=email, password=password)
            if user is None:
                messages.error(request, 'Неправильный пароль/email. Забыли пароль?')
                return redirect('/login')
            else:
                if remember == 1:
                    SESSION_COOKIE_AGE = 60 * 60 * 24 * 30
                else:
                    SESSION_COOKIE_AGE = 60 * 60 * 6

                auth.login(request, user)
                return redirect("/", request)
    else:
        form = LoginForm()
        context =  get_context(request, 'login')
        context.update({'telegram_login_widget': stuff.create_telegram_widget()})
        context.update({'form' : form})

    return render(request, 'login.html', context)


def logout(request):
    if request.user != 'AnonymousUser':
        auth.logout(request)
    return redirect("/")


def check(request):
    tg_login = stuff.create_telegram_widget()
    context = {'telegram_login_widget': tg_login}

    return render(request, 'check.html', context)


@public
def registration(request):
    if request.method == 'POST':
        try:
            form = RegisterForm(request.POST)
        except ValidationError as err:
            messages.error(request, err)
            return redirect('/register')

        if form.is_valid():
            form.clean()
            password = form.cleaned_data['password']
            email = form.cleaned_data['email']
            username = form.cleaned_data['username']

            pil_obj = get_avatar()
            user = User.objects.create_user(nick=username, email=email, password=password)
            update_avatar(pil_obj, user); user.save()
            form.send_or_confirm_mail(request, user, REG_CONF)

            return render(request, 'awaiting.html')
        else:
            raise Http404

    form = RegisterForm()
    context = get_context(request, 'login')
    context.update({'telegram_login_widget': stuff.create_telegram_widget()})
    context.update({'form': form})

    return render(request, 'register.html', context)


# stuff.clean_avatars(User.objects.all())

@login_required
def confirm_email(request):
    try:
        token = request.GET['token']
        token = Token.objects.get(token=token)
    except:
        raise Http404
    token.user.confirm_account(token); token.delete()
    messages.success(request, 'Ваша почта подтверждена, функции разблокированы')
    return redirect('/', request)



# DjangoBash Block
def create_user(name='test', mail='test@gm.com', password='password', is_super=False):
    user = None
    if not is_super:
        user = User.objects.create_user(email=mail, password=password, nick=name)
    else:
        user = User.objects.create_superuser(email=mail, password=password, nick=name)
    pil_obj = get_avatar();
    user.save()
    update_avatar(pil_obj, user)
    user.save()
