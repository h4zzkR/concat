from django.core.management.base import BaseCommand
from apps.main_app.views import create_user

class Command(BaseCommand):
    """
    Creates superuser
    """
    def handle(self, *args, **options):

        name = input('Name: '); mail = input('Email: '); password = input('Password: ')
        create_user(name=name, mail=mail, password=password, is_super=True)
        self.stdout.write(self.style.SUCCESS('Superuser created'))