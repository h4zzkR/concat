"""
Script for full clean migration
Not completed yet...
"""
import os
import sys

a = input('This action will clean all user data and migrations.\nDo you REALLY want to continue (Y, N)?: ')
print('Ok...') if a == 'Y'.lower() else sys.exit()
print('Cleaning...')
os.system('rm -rf /main_app/migrations')
os.system('rm -rf /main_app/__pycache__')
os.system('rm -rf /concat/__pycache__')
os.system('rm -rf /main_app/migrations')
os.system('rm -rf /profile_app/migrations')
os.system('rm -rf /tasks_logic_app/migrations')
# os.system('rm -rf /media/users')
os.system('rm db.sqlite3')
os.system('python manage.py flush')
print('Make zero migrations...')
os.system('python3 manage.py migrate admin zero')
os.system('python3 manage.py migrate auth zero')
os.system('python3 manage.py migrate contenttypes zero')
os.system('python3 manage.py migrate sessions zero')
os.system('python3 manage.py makemigrations main_app')
os.system('python3 manage.py makemigrations profile_app')
os.system('python3 manage.py makemigrations tasks_logic_app')
os.system('python3 manage.py migrate main_app')
os.system('python3 manage.py migrate profile_app')
os.system('python3 manage.py migrate tasks_logic_app')
os.system('python3 manage.py makemigrations')
os.system('python3 manage.py migrate')

#|| Uncomment if you will NOT load fixtures
#   By default in zero-fixture you will
#   find root@gmail.com:root account on dev.
os.system('python3 manage.py default_super')
# os.system('python3 manage.py loaddata fixtures/zero.json')
print("That's all")
