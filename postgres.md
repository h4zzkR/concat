# PostgreSQL setting up
**Safety first:**
```bash
python3 manage.py dumpdata > fixtures/datadump.json
 ```
 **Install postgres:**

```bash
 sudo apt-get install postgresql postgresql-server-dev-11
 sudo apt-get install python3-pip python3-dev libpq-dev postgresql-contrib
 ```
 
**In python env:**
```
    pip install psycopg2

```
 **Go to postgres terminal:**
```bash
sudo -u postgres psql
 ```
 
**In postgres:**
```bash;
CREATE DATABASE concat_db;
CREATE USER concat_admin WITH PASSWORD 'k2#@MWDEDeklsdwpckwk';
ALTER ROLE concat_admin SET client_encoding TO 'utf8';
ALTER ROLE concat_admin SET default_transaction_isolation TO 'read committed';
ALTER ROLE concat_admin SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE concat_db TO concat_admin;
\q
 ```
**Check if postgres in settings.py**

**With connected Postgres db:**
  ```bash
python manage.py makemigrations
python3 manage.py migrate --run-syncdb
python manage.py migrate
 ```
**Finally load base fixtures (or anything else):**
   ```bash
python3 manage.py loaddata fixtures/zero.json
 ```

#npm-Vue initialization
**In terminal:**
````npm install --save-dev axios bootstrap-sass jquery node-sass sass-loader vue webpack webpack-cli  @babel/core @babel/plugin-transform-runtime @babel/preset-env @babel/runtime babel-loader babel-preset-es2015 babel-preset-stage-0 css-loader style-loader vue-loader vue-hot-reload-api vue-template-compiler webpack-bundle-tracker````



