from apps.main_app.views import *
import apps.profile_app.views as tnpv
import apps.tasks_logic_app.views as tl
from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path
from django.conf.urls import include, url

from django.views.generic import TemplateView
from django.conf import settings

class ConnectionsView:
    template_name = 'allauth/account/connections.html'


urlpatterns = [
    path('admin/', admin.site.urls),
    path('third-accs-control/', third_accs_control),

    path('', index),
    path('main_app/', include('django.contrib.auth.urls')),
    path('u/<str:hash>', tnpv.render_profile),
    path('update_profile', tnpv.update_profile),
    path('confirm', confirm_email),
    path('login', login),
    path('test', tnpv.test),
    path('p/new', tl.new_project),

    path('p/<int:id>', tl.render_project),
    path('update', update_fields),
    path('update/telegram', update_fields_telegram),
    path('logout', logout),
    path('register', registration),
    path('check', check),
    path('gencon', tnpv.gen_con),
    path('accounts/', include('allauth.urls')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls)),]

# DEVELOPMENT ONLY

urlpatterns += [path('test', TemplateView.as_view(template_name='test.html'), name='test')]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
