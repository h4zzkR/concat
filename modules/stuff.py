import os
import random
import string
import uuid
from io import BytesIO

import requests
from PIL import Image
from django.conf import settings
from django.http import Http404
from django_telegram_login.widgets.constants import (
    MEDIUM,
    LARGE,
)
from django_telegram_login.widgets.generator import (
    create_callback_login_widget,
    create_redirect_login_widget,
)


def avatar_generator(size=10, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))


def token_generator():
    return uuid.uuid4().hex


def public(func_to_decorate):
    def new_func(*original_args, **original_kwargs):
        request = original_args[0]
        if not request.user.is_authenticated:
            return func_to_decorate(*original_args, **original_kwargs)
        else:
            raise Http404

    return new_func


def clean_avatars(objs):
    avatars = [str(i.picture)[str(i.picture).rfind('/') + 1:] for i in objs]
    for path in os.listdir(settings.MEDIA_ROOT + '/users/avatars'):
        if path not in avatars and path != 'default.png':
            os.remove(settings.MEDIA_ROOT + '/users/avatars/' + path)


def parse_google_data(query):
    query = query[0]['extra_data']
    type = 'google'
    try:
        pic = query['picture']
    except KeyError:
        pic = query['avatar_url']
        github_link = query['html_url']
        type = 'github'
    response = requests.get(pic)
    img = Image.open(BytesIO(response.content))
    if type == 'google':
        return img, type
    else:
        return img, github_link


def get_tg_pic(url):
    response = requests.get(url)
    return Image.open(BytesIO(response.content))


def get_url_path(request):
    return 'http://' + request.META['HTTP_HOST']


def create_telegram_widget():
    bot_name = settings.TELEGRAM_BOT_NAME
    bot_token = settings.TELEGRAM_BOT_TOKEN
    redirect_url = settings.TELEGRAM_LOGIN_REDIRECT_URL

    telegram_login_widget = create_callback_login_widget(bot_name, corner_radius=10, size=MEDIUM)

    telegram_login_widget = create_redirect_login_widget(
        redirect_url, bot_name, size=LARGE
    )
    return telegram_login_widget
