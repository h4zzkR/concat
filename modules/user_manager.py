from django.contrib.auth.base_user import BaseUserManager
import uuid


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, nick, is_super, is_admin):
        """
        Создает и сохраняет пользователя с введенным им email и паролем.
        """
        if not email:
            raise ValueError('email должен быть указан!')
        user = self.model(email=email, username=nick, is_admin=is_admin, is_superuser=is_super, is_staff=is_super)
        if is_super == True:
            user.is_confirmed = True
        user.set_password(password)
        user.save(using=self._db)
        user.hash = uuid.uuid4()
        return user

    def create_user(self, email, password, nick):
        return self._create_user(email, password, nick, False, False)

    def create_superuser(self, email, password, nick):
        return self._create_user(email, password, nick, True, True)
